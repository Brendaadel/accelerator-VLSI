LIBRARY IEEE;
USE IEEE.std_logic_1164.all;

ENTITY SRLatch IS  
PORT (set , reset,clk:IN std_logic;
       OUT1: OUT std_logic
);   
END ENTITY SRLatch;


ARCHITECTURE SRLatch OF SRLatch IS
BEGIN
PROCESS (clk,reset,set)
BEGIN

if(reset='1' AND clk='1') THEN
OUT1<='0';
elsif ((clk='1') AND set='1') THEN
OUT1<='1';
END IF;
END PROCESS;     
END SRLatch;

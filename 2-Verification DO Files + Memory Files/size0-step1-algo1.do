mem load -filltype inc -filldata 0 -fillradix symbolic -skip 0 /accelerator/myRam/ram

force -freeze sim:/accelerator/clk 1 0, 0 {50 ps} -r 100
force -freeze sim:/accelerator/size 0 0
force -freeze sim:/accelerator/step 1 0
force -freeze sim:/accelerator/algo 1 0
force -freeze sim:/accelerator/resetSig 1 0
run
force -freeze sim:/accelerator/resetSig 0 0
force -freeze sim:/accelerator/start 1 0
run
force -freeze sim:/accelerator/start 0 0
run 38709800
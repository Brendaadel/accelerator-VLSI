add wave -position insertpoint  \
sim:/accelerator/clk \
sim:/accelerator/size \
sim:/accelerator/step \
sim:/accelerator/algo \
sim:/accelerator/resetSig \
sim:/accelerator/start
add wave -position insertpoint  \
sim:/accelerator/counter
add wave -position insertpoint  \
sim:/accelerator/done \
sim:/accelerator/readFilter \
sim:/accelerator/readWindow \
sim:/accelerator/saveResult
add wave -position insertpoint  \
sim:/accelerator/cacheLabel/reg1/q
add wave -position insertpoint  \
sim:/accelerator/cacheLabel/reg2/q
add wave -position insertpoint  \
sim:/accelerator/cacheLabel/reg3/q
add wave -position insertpoint sim:/accelerator/cacheLabel/reg4/q
add wave -position insertpoint  \
sim:/accelerator/cacheLabel/reg4/q
add wave -position insertpoint  \
sim:/accelerator/cacheLabel/reg5/q
add wave -position insertpoint sim:/accelerator/cacheLabel/reg6/q
add wave -position insertpoint  \
sim:/accelerator/cacheLabel/reg7/q
add wave -position insertpoint  \
sim:/accelerator/cacheLabel/reg8/q
add wave -position insertpoint  \
sim:/accelerator/cacheLabel/reg9/q
add wave -position insertpoint  \
sim:/accelerator/cacheLabel/reg10/q
add wave -position insertpoint  \
sim:/accelerator/DmaLabel/fsmC/AddressRegister/q
add wave -position insertpoint  \
sim:/accelerator/DmaLabel/mainAddressReg/q
add wave -position insertpoint  \
sim:/accelerator/aluWithSumm/outAlu
add wave -position insertpoint  \
sim:/accelerator/myRam/dataIN \
sim:/accelerator/myRam/Wr
add wave -position end  sim:/accelerator/DmaLabel/counterLabel/qReg2
add wave -position end  sim:/accelerator/DmaLabel/counterLabel/rstSignalCounter
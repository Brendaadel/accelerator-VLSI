library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


ENTITY ALU IS
GENERIC ( n : integer := 8);
PORT( clk,algo,rst : IN std_logic;
  filter,window : IN std_logic_vector(n-1 DOWNTO 0);
  outAlu : OUT std_logic_vector(n-1 DOWNTO 0);
  nodeBetaOut : OUT std_logic
  
);
END ALU;


ARCHITECTURE ALU_Arch OF ALU IS
 
COMPONENT BoothCounter IS
GENERIC ( n : integer := 3);
PORT( clk,rst: IN std_logic;
 outPort : INOUT std_logic_vector(n-1 DOWNTO 0)
);
END COMPONENT;

Signal busA,busS,busP : std_logic_vector(2*n DOWNTO 0);
Signal counterSignal : std_logic_vector(2 DOWNTO 0);
Signal nodeBeta,aluClk : std_logic;


  
BEGIN

aluClk <= clk AND algo;
nodeBeta <= '1' WHEN counterSignal="111" AND aluClk='0' ELSE '0';
nodeBetaOut <= '1' WHEN counterSignal="111" AND aluClk='0' ELSE '0';
outAlu <= window WHEN algo='0' 
	ELSE busP(n downto 1) WHEN nodeBeta='1' AND  busP(2*n downto n-1)=std_logic_vector(to_unsigned(0,n))
	ELSE std_logic_vector(to_unsigned(255,n)) WHEN nodeBeta='1'
	ELSE std_logic_vector(to_unsigned(0,n));

busA <= window&std_logic_vector(to_unsigned(0,n+1));
busS <= std_logic_vector(unsigned(not window) +1)&std_logic_vector(to_unsigned(0,n+1));

 
PROCESS (aluClk)
BEGIN
  
   IF rst = '1' THEN
     busP<=std_logic_vector(to_unsigned(0,(2*n)+1));
  ELSE
    
  IF rising_edge(aluClk) THEN
        
    IF busP(1 downto 0)="10" THEN				
      busP <= std_logic_vector(unsigned(busP) + unsigned(busS));
    ELSIF busP(1 downto 0)="01" THEN
      busP <= std_logic_vector(unsigned(busP) + unsigned(busA));
    ELSIF counterSignal="000" AND busP=std_logic_vector(to_unsigned(0,(2*n)+1)) THEN 
      
      IF filter(0)='1' THEN 
        busP <= std_logic_vector(unsigned(std_logic_vector(to_unsigned(0,n))&filter&'0') + unsigned(busS));
      ELSE
        busP <= std_logic_vector(to_unsigned(0,n))&filter&'0';
      END IF;
      END IF;
    
  END IF;
  
  IF falling_edge(aluClk) THEN  
    busP <=  busP(2*n)&busP(2*n downto 1) ;
  END IF;
END IF;		
END PROCESS;



counterLoop : BoothCounter PORT MAP (aluClk,rst,counterSignal);


END ALU_Arch;




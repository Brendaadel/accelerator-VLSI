LIBRARY IEEE;
USE IEEE.std_logic_1164.all;

ENTITY cache IS  
PORT ( clk , reset , size:  IN std_logic;
       enables :IN std_logic_vector (10 downto 0);
       dataIN :  IN std_logic_vector (39 downto 0); -- 5 bytes each time * 8 = 40 bit 
       outReg1 , outReg2 , outReg3 , outReg4 , outReg5 , outReg6 , outReg7, outReg8 , outReg9 , outReg10 , outReg11: OUT std_logic_vector (39 downto 0)
     
);   
END ENTITY cache ;


ARCHITECTURE cache OF cache IS
COMPONENT DFlipFlop IS
GENERIC ( n : integer := 32);
PORT( Clk,Rst,EnableDFF  : IN std_logic;
d : IN std_logic_vector(n-1 DOWNTO 0);
 q : OUT std_logic_vector(n-1 DOWNTO 0)
);
END COMPONENT ;
Signal data  : std_logic_vector (39 downto 0);
BEGIN
data<=  ("0000000000000000"& dataIN(23 downto 0)) WHEN size='0'
else dataIN;     
reg1:DFlipFlop GENERIC MAP (n=>40) PORT MAP (clk,reset,enables(0),data,outReg1);  
reg2:DFlipFlop GENERIC MAP (n=>40) PORT MAP (clk,reset,enables(1),data,outReg2);
reg3:DFlipFlop GENERIC MAP (n=>40) PORT MAP (clk,reset,enables(2),data,outReg3);
reg4:DFlipFlop GENERIC MAP (n=>40) PORT MAP (clk,reset,enables(3),data,outReg4);
reg5:DFlipFlop GENERIC MAP (n=>40) PORT MAP (clk,reset,enables(4),data,outReg5);
reg6:DFlipFlop GENERIC MAP (n=>40) PORT MAP (clk,reset,enables(5),data,outReg6);
reg7:DFlipFlop GENERIC MAP (n=>40) PORT MAP (clk,reset,enables(6),data,outReg7);
reg8:DFlipFlop GENERIC MAP (n=>40) PORT MAP (clk,reset,enables(7),data,outReg8);
reg9:DFlipFlop GENERIC MAP (n=>40) PORT MAP (clk,reset,enables(8),data,outReg9);
reg10:DFlipFlop GENERIC MAP (n=>40) PORT MAP (clk,reset,enables(9),data,outReg10);
--reg11:DFlipFlop GENERIC MAP (n=>40) PORT MAP (clk,reset,enables(10),data,outReg11);
outReg11<=data;


                 
END cache;

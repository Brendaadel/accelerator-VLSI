
library ieee;
use ieee.std_logic_1164.all;
USE IEEE.numeric_std.all;

ENTITY FSM IS
GENERIC ( n : integer := 18);
PORT( clk,rst,step,size,noBorder,readWindow,readFilter,addressRegisterENB  : IN std_logic;
  addressRegisterQ,nodeAlpha : IN std_logic_vector(n-1 DOWNTO 0);
  counter : In std_logic_vector(2 DOWNTO 0);
 addressRegisterD,windowCounter : OUT std_logic_vector(n-1 DOWNTO 0)
);
END FSM;


ARCHITECTURE FSM_Arch OF FSM IS
  
COMPONENT mux2 IS  
GENERIC (n : integer := 1);
PORT (SEl	:  IN std_logic;
       IN1,IN2 :IN std_logic_vector (n-1 downto 0);
       OUT1: OUT  std_logic_vector (n-1 downto 0)
);     
END COMPONENT;


COMPONENT mux4 IS  
GENERIC (n : integer := 2);
     PORT(SEL1,SEL2: IN std_logic;
	  in1,in2,in3,in4: IN std_logic_vector(n-1 downto 0);
	  output: OUT std_logic_vector(n-1 downto 0));
END COMPONENT;

COMPONENT my_nadder IS
       GENERIC (n : integer := 16);
PORT(a,b : IN std_logic_vector(n-1  DOWNTO 0);
            cin : IN std_logic;  
            s : OUT std_logic_vector(n-1 DOWNTO 0);    
            cout : OUT std_logic);
END COMPONENT;

COMPONENT DFlipFlop IS
GENERIC ( n : integer := 32);
PORT( Clk,Rst,EnableDFF  : IN std_logic;
d : IN std_logic_vector(n-1 DOWNTO 0);
 q : OUT std_logic_vector(n-1 DOWNTO 0)
);
END COMPONENT;

  
SIGNAL outMux1,outMux2,outMux3,outMux4,outAdder,outRegister,outIncMux,outIncOrZeroMux,outIncAdder : std_logic_vector(17 downto 0);
SIGNAL adderCout,outIncOrZeroENB,finalMuxENB,nodeAlphaOneBit : std_logic;

  
BEGIN

nodeAlphaOneBit <= '0' WHEN nodeAlpha = std_logic_vector(to_unsigned(0,18)) ELSE '1';
outIncOrZeroENB <= readFilter AND ( not counter(0) AND not counter(1) AND not counter(2));
finalMuxENB <= readWindow AND ( not counter(0) AND not counter(1) AND not counter(2));

--first line
Mux1C : mux2 GENERIC MAP (n=>18) PORT MAP (step,std_logic_vector(to_unsigned(1,18)),std_logic_vector(to_unsigned(2,18)),outMux1);
Mux2C : mux4 GENERIC MAP (n=>18) PORT MAP (step,size,std_logic_vector(to_unsigned(3,18)),std_logic_vector(to_unsigned(4,18)),std_logic_vector(to_unsigned(5,18)),std_logic_vector(to_unsigned(6,18)),outMux2);
  
Mux1_2 : mux2 GENERIC MAP (n=>18) PORT MAP (noBorder,outMux1,outMux2,outMux3);
Mux1_2OrZero : mux2 GENERIC MAP (n=>18) PORT MAP (nodeAlphaOneBit,std_logic_vector(to_unsigned(0,18)),outMux3,outMux4);

AddressAdder : my_nadder GENERIC MAP (n=>18) PORT MAP (outMux4,outRegister,'0',outAdder,adderCout);

AddressRegister : DFlipFlop GENERIC MAP (n=>18)  PORT MAP (clk,rst,finalMuxENB,outAdder,outRegister);


--second line
MuxInccrement : mux4 GENERIC MAP (n=>18) PORT MAP (size,readWindow,std_logic_vector(to_unsigned(3,18)),std_logic_vector(to_unsigned(5,18)),std_logic_vector(to_unsigned(256,18)),std_logic_vector(to_unsigned(256,18)),outIncMux);
MuxIncOrZero : mux2 GENERIC MAP (n=>18) PORT MAP (outIncOrZeroENB,outIncMux,std_logic_vector(to_unsigned(0,18)),outIncOrZeroMux);

IncAdder : my_nadder GENERIC MAP (n=>18) PORT MAP (outIncOrZeroMux,addressRegisterQ,'0',outIncAdder,adderCout); --need enable

finalMux : mux2 GENERIC MAP (n=>18) PORT MAP (finalMuxENB,outIncAdder,outAdder,addressRegisterD);
  
windowCounter <= outRegister;

END FSM_Arch;


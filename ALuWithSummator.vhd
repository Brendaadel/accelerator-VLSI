
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


ENTITY ALUSUMMATOR IS
GENERIC ( n : integer := 8);
PORT( clk,algo,rst,size,enable : IN std_logic;
  IN1,IN2,IN3,IN4,IN5,IN6,IN7,IN8,IN9,IN10 : IN std_logic_vector((n*5)-1 DOWNTO 0);
  outAlu : OUT std_logic_vector(n-1 DOWNTO 0);
  nodeBetaOut : OUT std_logic
  
);
END ALUSUMMATOR;

ARCHITECTURE ALUSUMMATOR_arch OF ALUSUMMATOR IS
COMPONENT ALU IS
GENERIC ( n : integer := 8);
PORT( clk,algo,rst : IN std_logic;
  filter,window : IN std_logic_vector(n-1 DOWNTO 0);
  outAlu : OUT std_logic_vector(n-1 DOWNTO 0);
  nodeBetaOut : OUT std_logic
  
);
END COMPONENT;

COMPONENT summator IS
GENERIC ( n : integer := 8);
PORT( algo,size: IN std_logic;
  IN1,IN2,IN3,IN4,IN5,IN6,IN7,IN8,IN9,IN10,IN11,IN12,IN13,IN14,IN15,IN16,IN17,IN18,IN19,IN20,IN21,IN22,IN23,IN24,IN25: IN std_logic_vector(n-1 DOWNTO 0);
 outPort : OUT std_logic_vector(n-1 DOWNTO 0)
);
END COMPONENT;

Signal OUT1,OUT2,OUT3,OUT4,OUT5,OUT6,OUT7,OUT8,OUT9,OUT10,OUT11,OUT12,OUT13,OUT14,OUT15,OUT16,OUT17,OUT18,OUT19,OUT20,OUT21,OUT22,OUT23,OUT24,OUT25 : std_logic_vector(n-1 DOWNTO 0);
Signal nodeBetaDummy,aluClk : std_logic;


  
BEGIN

aluClk <= clk AND enable;


ALU1  : ALU PORT MAP (aluClk,algo,rst,IN1(39 downto 32),IN6(39 downto 32),OUT1,nodeBetaOut);
ALU2  : ALU PORT MAP (aluClk,algo,rst,IN1(31 downto 24),IN6(31 downto 24),OUT2,nodeBetaDummy);
ALU3  : ALU PORT MAP (aluClk,algo,rst,IN1(23 downto 16),IN6(23 downto 16),OUT3,nodeBetaDummy);
ALU4  : ALU PORT MAP (aluClk,algo,rst,IN1(15 downto 8),IN6(15 downto 8),OUT4,nodeBetaDummy);
ALU5  : ALU PORT MAP (aluClk,algo,rst,IN1(7 downto 0),IN6(7 downto 0),OUT5,nodeBetaDummy);
ALU6  : ALU PORT MAP (aluClk,algo,rst,IN2(39 downto 32),IN7(39 downto 32),OUT6,nodeBetaDummy);
ALU7  : ALU PORT MAP (aluClk,algo,rst,IN2(31 downto 24),IN7(31 downto 24),OUT7,nodeBetaDummy);
ALU8  : ALU PORT MAP (aluClk,algo,rst,IN2(23 downto 16),IN7(23 downto 16),OUT8,nodeBetaDummy);
ALU9  : ALU PORT MAP (aluClk,algo,rst,IN2(15 downto 8),IN7(15 downto 8),OUT9,nodeBetaDummy);
ALU10 : ALU PORT MAP (aluClk,algo,rst,IN2(7 downto 0),IN7(7 downto 0),OUT10,nodeBetaDummy);
ALU11 : ALU PORT MAP (aluClk,algo,rst,IN3(39 downto 32),IN8(39 downto 32),OUT11,nodeBetaDummy);
ALU12 : ALU PORT MAP (aluClk,algo,rst,IN3(31 downto 24),IN8(31 downto 24),OUT12,nodeBetaDummy);
ALU13 : ALU PORT MAP (aluClk,algo,rst,IN3(23 downto 16),IN8(23 downto 16),OUT13,nodeBetaDummy);
ALU14 : ALU PORT MAP (aluClk,algo,rst,IN3(15 downto 8),IN8(15 downto 8),OUT14,nodeBetaDummy);
ALU15 : ALU PORT MAP (aluClk,algo,rst,IN3(7 downto 0),IN8(7 downto 0),OUT15,nodeBetaDummy);
ALU16 : ALU PORT MAP (aluClk,algo,rst,IN4(39 downto 32),IN9(39 downto 32),OUT16,nodeBetaDummy);
ALU17 : ALU PORT MAP (aluClk,algo,rst,IN4(31 downto 24),IN9(31 downto 24),OUT17,nodeBetaDummy);
ALU18 : ALU PORT MAP (aluClk,algo,rst,IN4(23 downto 16),IN9(23 downto 16),OUT18,nodeBetaDummy);
ALU19 : ALU PORT MAP (aluClk,algo,rst,IN4(15 downto 8),IN9(15 downto 8),OUT19,nodeBetaDummy);
ALU20 : ALU PORT MAP (aluClk,algo,rst,IN4(7 downto 0),IN9(7 downto 0),OUT20,nodeBetaDummy);
ALU21 : ALU PORT MAP (aluClk,algo,rst,IN5(39 downto 32),IN10(39 downto 32),OUT21,nodeBetaDummy);
ALU22 : ALU PORT MAP (aluClk,algo,rst,IN5(31 downto 24),IN10(31 downto 24),OUT22,nodeBetaDummy);
ALU23 : ALU PORT MAP (aluClk,algo,rst,IN5(23 downto 16),IN10(23 downto 16),OUT23,nodeBetaDummy);
ALU24 : ALU PORT MAP (aluClk,algo,rst,IN5(15 downto 8),IN10(15 downto 8),OUT24,nodeBetaDummy);
ALU25 : ALU PORT MAP (clk,algo,rst,IN5(7 downto 0),IN10(7 downto 0),OUT25,nodeBetaDummy);
  
Summ_C : summator PORT MAP (algo,size,OUT1,OUT2,OUT3,OUT4,OUT5,OUT6,OUT7,OUT8,OUT9,OUT10,OUT11,OUT12,OUT13,OUT14,OUT15,OUT16,OUT17,OUT18,OUT19,OUT20,OUT21,OUT22,OUT23,OUT24,OUT25,outAlu);


END ALUSUMMATOR_arch;





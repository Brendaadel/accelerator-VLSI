LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
ENTITY MUX4 IS
	generic(n:integer:=32);
     PORT(SEL1,SEL2: IN std_logic;
	  in1,in2,in3,in4: IN std_logic_vector(n-1 downto 0);
	  output: OUT std_logic_vector(n-1 downto 0));
END MUX4;

ARCHITECTURE arch OF MUX4 IS
BEGIN
output <= in1 WHEN SEL1='0' and SEL2='0' ELSE
	  in2 WHEN SEL1='1' and SEL2='0' ELSE
          in3 WHEN SEL1='0' and SEL2='1' ELSE
	  in4;
END arch;

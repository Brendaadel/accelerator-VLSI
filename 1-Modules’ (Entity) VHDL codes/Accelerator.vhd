LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
USE IEEE.numeric_std.all;
ENTITY Accelerator IS  
PORT ( clk , size ,step , algo , resetSig ,start:  IN std_logic
);   
END ENTITY Accelerator ;


ARCHITECTURE Accelerator  OF Accelerator  IS
COMPONENT cache IS  
PORT ( clk , reset , size:  IN std_logic;
       enables :IN std_logic_vector (10 downto 0);
       dataIN :  IN std_logic_vector (39 downto 0); -- 5 bytes each time * 8 = 40 bit 
       outReg1 , outReg2 , outReg3 , outReg4 , outReg5 , outReg6 , outReg7, outReg8 , outReg9 , outReg10 , outReg11: OUT std_logic_vector (39 downto 0)
     
);   
END COMPONENT ;
COMPONENT DMA IS  
PORT (clk,rst ,filterRead,windowRead,start,resultAck, filterSize , stepSize,saveResult:  IN std_logic;
      filterAck , dataAck, signalDone,nodeGamma3,writeSignalRam : OUT std_logic;
      outAddressRamSignal: OUT std_logic_vector (17 downto 0);
      counter : OUT std_logic_vector (2 downto 0)
);     
END COMPONENT DMA;

COMPONENT cacheController IS  
PORT (size , readFilter , readWindow , aluDone:  IN std_logic;
       counter:IN std_logic_vector (2 downto 0);
       regEnables: OUT  std_logic_vector (10 downto 0)
);   
END  COMPONENT;

COMPONENT Ram IS
	PORT(
		clk: in std_logic;
		address : IN  std_logic_vector(17 DOWNTO 0);
                dataIN : IN std_logic_vector(7 DOWNTO 0);
		dataOUT : OUT std_logic_vector(39 DOWNTO 0);
		Wr:IN std_logic);
END COMPONENT;

COMPONENT handShakes IS  
PORT (start,filterAck , dataAck ,done,algo,nodeB,clk ,reset:  IN std_logic;
      readFilter , readWindow ,resultAck , saveResult,aluDn,enableAlu2:  OUT std_logic
       
);    
END COMPONENT handShakes;

COMPONENT ALUSUMMATOR IS
GENERIC ( n : integer := 8);
PORT( clk,algo,rst,size,enable : IN std_logic;
  IN1,IN2,IN3,IN4,IN5,IN6,IN7,IN8,IN9,IN10 : IN std_logic_vector((n*5) DOWNTO 0);
  outAlu : OUT std_logic_vector(n-1 DOWNTO 0);
  nodeBetaOut : OUT std_logic
  
);
END COMPONENT;

COMPONENT DFlipFlop IS
GENERIC ( n : integer := 32);
PORT( Clk,Rst,EnableDFF  : IN std_logic;
d : IN std_logic_vector(n-1 DOWNTO 0);
 q : OUT std_logic_vector(n-1 DOWNTO 0)
);
END COMPONENT;
 
SIGNAL enablesCache : std_logic_vector (10 downto 0);
SIGNAL counter :std_logic_vector (2 downto 0);
SIGNAL dataINRamSig,outAlu :std_logic_vector(7 downto 0);
SIGNAL dataInCache,dataInCacheFromRam : std_logic_vector (39 downto 0);
SIGNAL outAddressRamSig : std_logic_vector (17 downto 0);
SIGNAL doneLatched , doneVector : std_logic_vector (0 downto 0);
SIGNAL resetSigDone,reset,writeSignalRamSig ,notClk,filterAck, dataAck , done , readFilter,readWindow,resultAck , saveResult , aluDone,nodeB,enableAlu,nodeGamma,nodeGammaSig : std_logic;
SIGNAL outReg1Sig , outReg2Sig , outReg3Sig , outReg4Sig , outReg5Sig , outReg6Sig , outReg7Sig, outReg8Sig , outReg9Sig , outReg10Sig , outReg11Sig: std_logic_vector (39 downto 0);

BEGIN   

PROCESS (clk)
BEGIN

if(clk='1') THEN
nodeGammaSig<=nodeGamma;
ELSE
nodeGammaSig<='0';
END IF;
END PROCESS;  


myRam: Ram PORT MAP (clk,outAddressRamSig, dataINRamSig ,dataInCacheFromRam,writeSignalRamSig);
cacheLabel : cache PORT MAP (clk , reset , size ,enablesCache ,dataInCache , outReg1Sig , outReg2Sig , outReg3Sig , outReg4Sig , outReg5Sig , outReg6Sig , outReg7Sig, outReg8Sig , outReg9Sig , outReg10Sig , outReg11Sig);  
cacheControllerLabel : cacheController PORT MAP (size , readFilter , readWindow , aluDone , counter,  enablesCache);
DmaLabel: DMA port MAP (clk,reset ,readFilter,readWindow,start,resultAck, size , step,saveResult, filterAck , dataAck, done,nodeGamma,writeSignalRamSig,outAddressRamSig ,counter);  
handshake: handShakes port map (start,filterAck , dataAck,done , algo ,nodeB,clk,reset,readFilter,readWindow,resultAck , saveResult,aluDone,enableAlu); 
aluWithSumm : ALUSUMMATOR generic map (n=>8)port map (clk,algo,nodeGammaSig,size,enableAlu,outReg1Sig,outReg2Sig,outReg3Sig,outReg4Sig,outReg5Sig,outReg6Sig,outReg7Sig,outReg8Sig,outReg9Sig,outReg10Sig,outAlu,nodeB);
dataInCache <= std_logic_vector(to_unsigned(0,32))&outAlu WHEN aluDone='1' ELSE dataInCacheFromRam;
dataInRamSig<=outAlu;
reset<= resetSig OR doneLatched(0);
notClk <=not(clk);
doneVector(0)<=done;
resetSigDone<=reset AND notCLK;
latchSignalDone:DFlipFlop GENERIC MAP (n=>1) PORT MAP (clk,resetSigDone,'1',doneVector,doneLatched);
END Accelerator ;

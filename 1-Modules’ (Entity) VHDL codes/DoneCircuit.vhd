LIBRARY IEEE;
USE IEEE.std_logic_1164.all;

ENTITY DoneCircuit IS  
PORT (resultAck , filterSize , stepSize:  IN std_logic;
       addressSelOfMemory :IN std_logic_vector (17 downto 0);
      doneSignal: OUT  std_logic
);   
END ENTITY DoneCircuit;


ARCHITECTURE DoneCircuit OF DoneCircuit IS
SIGNAL outXnor1 , outXnor2 , outXnor3 , outXnor4: std_logic_vector(17 downto 0) ;
SIGNAL  num1 ,num2,num3,num4  : std_logic_vector (17 downto 0);
SIGNAL outAnd1 , outAnd2,outAnd3,outAnd4 : std_logic ;
BEGIN

---x""
num1<="00"&"1111111000000000";
outXnor1 <= addressSelOfMemory XNOR num1;
outAnd1<='1' WHEN (filterSize='0' AND stepSize='0' AND outXnor1="111111111111111111") 
ELSE '0';

num2<="00"&"1111110111111100";
outXnor2 <= addressSelOfMemory XNOR num2;
outAnd2<='1' WHEN (filterSize='0' AND stepSize='1' AND outXnor2="111111111111111111") 
ELSE '0';

num3<="00"&"1111101111111011";
outXnor3 <= addressSelOfMemory XNOR num3;
outAnd3<='1' WHEN (filterSize='1' AND stepSize='0' AND outXnor3="111111111111111111") 
ELSE '0';

num4<="00"&"1111101111111010";
outXnor4 <= addressSelOfMemory XNOR num4;
outAnd4<='1' WHEN (filterSize='1' AND stepSize='1' AND outXnor4="111111111111111111") 
ELSE '0';     


doneSignal <= (outAnd1 or outAnd2 or outAnd3 or outAnd4);

     
END DoneCircuit;
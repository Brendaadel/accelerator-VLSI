library ieee;
use ieee.std_logic_1164.all;
ENTITY DFlipFlop IS
GENERIC ( n : integer := 32);
PORT( Clk,Rst,EnableDFF  : IN std_logic;
d : IN std_logic_vector(n-1 DOWNTO 0);
 q : OUT std_logic_vector(n-1 DOWNTO 0)
);
END DFlipFlop;


ARCHITECTURE DFlipFlop OF DFlipFlop IS
BEGIN
PROCESS (Rst,Clk)
BEGIN

IF Rst = '1' THEN
		q <= (OTHERS=>'0');
ELSIF (Clk='1' AND EnableDFF = '1')THEN
		q <= d;
END IF;
END PROCESS;
END DFlipFlop;

library ieee;
use ieee.std_logic_1164.all;
entity myTristate is
GENERIC (n : integer := 1);
port (myIn:in std_logic_vector (n-1 downto 0);
control:in std_logic;
myOut: out std_logic_vector (n-1 downto 0)
);
end entity myTristate;

Architecture myTristate of myTristate is 
SIGNAL myZ:std_logic_vector(n-1 downto 0) ;
begin 
myZ<=(OTHERS=>'Z');
myOut<= myIn WHEN control='1'
  else myZ;


end myTristate ;
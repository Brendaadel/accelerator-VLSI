LIBRARY IEEE;
USE IEEE.std_logic_1164.all;

ENTITY SRFlipFlop IS  
PORT (set , reset,clk:IN std_logic;
       OUT1: OUT std_logic
);   
END ENTITY SRFlipFlop;


ARCHITECTURE SRFlipFlop OF SRFlipFlop IS
BEGIN
PROCESS (clk)
BEGIN
if(reset='1') THEN
OUT1<='0';
elsif (rising_edge(clk) AND set='1') THEN
OUT1<='1';
END IF;
END PROCESS;     
END SRFlipFlop;


LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
-- n-bit adder
ENTITY my_nadder_ov IS
       GENERIC (n : integer := 8);
PORT(a,b : IN std_logic_vector(n-1  DOWNTO 0);
            cin : IN std_logic;  
            s : OUT std_logic_vector(n-1 DOWNTO 0);    
            cout : OUT std_logic);
END my_nadder_ov;
ARCHITECTURE a_my_nadder_ov OF my_nadder_ov IS
      COMPONENT my_adder IS
              PORT( a,b,cin : IN std_logic; 
                        s,cout : OUT std_logic);
        END COMPONENT;
SIGNAL temp : std_logic_vector(n-1 DOWNTO 0);
SIGNAL ss : std_logic_vector(n-1 DOWNTO 0);
BEGIN
  f0: my_adder PORT MAP(a(0),b(0),cin,ss(0),temp(0));
  loop1: FOR i IN 1 TO n-1 GENERATE
          fx: my_adder PORT MAP  (a(i),b(i),temp(i-1),ss(i),temp(i));
    END GENERATE;
s<= std_logic_vector(to_unsigned(255,n)) WHEN temp(n-1)='1' ELSE ss;
    cout <= temp(n-1);
END a_my_nadder_ov;


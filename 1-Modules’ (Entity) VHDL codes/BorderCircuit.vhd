
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


ENTITY BorderCircuit IS
GENERIC ( n : integer := 18);
PORT( step,size : IN std_logic;
  addressRegisterQ : IN std_logic_vector(n-1 DOWNTO 0);
 isBorder : OUT std_logic
);
END BorderCircuit;


ARCHITECTURE BorderCircuit_Arch OF BorderCircuit IS
 

  
SIGNAL addressMod256 : integer := 0;

  
BEGIN
  
  
addressMod256 <= (to_integer(unsigned(addressRegisterQ)) mod 256);

isBorder <=   '1'  WHEN ((addressMod256=253  AND size ='0'  AND step ='0') OR
                        (addressMod256=252  AND size ='0'  AND step ='1') OR
                        (addressMod256=251  AND size ='1'  AND step ='0') OR
                        (addressMod256=250  AND size ='1'  AND step ='1'))
ELSE '0';

  
END BorderCircuit_Arch;



LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
USE IEEE.numeric_std.all;

ENTITY DMA IS  
PORT (clk,rst ,filterRead,windowRead,start,resultAck, filterSize , stepSize,saveResult:  IN std_logic;
      filterAck , dataAck, signalDone,nodeGamma3,writeSignalRam : OUT std_logic;
      outAddressRamSignal: OUT std_logic_vector (17 downto 0);
      counter : OUT std_logic_vector (2 downto 0)
);   
END ENTITY DMA;


ARCHITECTURE DMA OF DMA IS

COMPONENT CounterGrid IS
PORT( clk,rst,enb,filterRead,windowRead,start,size,saveResult,enableMainAddressReg: IN std_logic;
filterAck , dataAck ,nodeGamma2 : OUT std_logic;
counterOut: OUT std_logic_vector (2 downto 0)
);
END COMPONENT;

COMPONENT DoneCircuit IS
PORT (resultAck , filterSize , stepSize:  IN std_logic;
       addressSelOfMemory :IN std_logic_vector (17 downto 0);
      doneSignal: OUT  std_logic
);   
END COMPONENT ;

COMPONENT FSM IS
GENERIC ( n : integer := 18);
PORT( clk,rst,step,size,noBorder,readWindow,readFilter,addressRegisterENB  : IN std_logic;
  addressRegisterQ,nodeAlpha : IN std_logic_vector(n-1 DOWNTO 0);
  counter : In std_logic_vector(2 DOWNTO 0);
 addressRegisterD,windowCounter : OUT std_logic_vector(n-1 DOWNTO 0)
);
END COMPONENT ;

COMPONENT BorderCircuit IS 
GENERIC ( n : integer := 18);
PORT( step,size : IN std_logic;
  addressRegisterQ : IN std_logic_vector(n-1 DOWNTO 0);
 isBorder : OUT std_logic
);
END COMPONENT;

COMPONENT DFlipFlop IS
GENERIC ( n : integer := 32);
PORT( Clk,Rst,EnableDFF  : IN std_logic;
d : IN std_logic_vector(n-1 DOWNTO 0);
 q : OUT std_logic_vector(n-1 DOWNTO 0)
);
END COMPONENT ;

COMPONENT my_nadder IS
       GENERIC (n : integer := 16);
PORT(a,b : IN std_logic_vector(n-1  DOWNTO 0);
            cin : IN std_logic;  
            s : OUT std_logic_vector(n-1 DOWNTO 0);    
            cout : OUT std_logic);
END COMPONENT;

COMPONENT mux2 IS  
GENERIC (n : integer := 1);
PORT (SEl	:  IN std_logic;
       IN1,IN2 :IN std_logic_vector (n-1 downto 0);
       OUT1: OUT  std_logic_vector (n-1 downto 0)
);   
END COMPONENT;
COMPONENT Ram is 
PORT(
		clk: in std_logic;
		address : IN  std_logic_vector(17 DOWNTO 0);
                dataIN : IN std_logic_vector(7 DOWNTO 0);
		dataOUT : OUT std_logic_vector(39 DOWNTO 0);
		Wr:IN std_logic);
END COMPONENT;


SIGNAL  enableMainAddressReg,windowReadAndFalling, outBorderCircuit,adderCout,notClk,nodeGamma:std_logic;
SIGNAL counterOut : std_logic_vector (2 downto 0);
SIGNAL dataOUTRamSig : std_logic_vector (39 downto 0);
SIGNAL MainAddressRegQ,MainAddressRegD ,nodeAlpha,windowCounterSignal,outAdderValue,outReadAddress,outWriteAddressAdder,writeLocationRegisterD:std_logic_vector(17 downto 0);

BEGIN
notClk <= not clk;
mainAddressReg: DFlipFlop GENERIC MAP (n=>18)PORT MAP(notClk,rst,enableMainAddressReg,MainAddressRegD,MainAddressRegQ);   
fsmC: FSM  GENERIC MAP (n=>18) PORT MAP( clk,rst,stepSize,filterSize,outBorderCircuit,windowRead,filterRead,enableMainAddressReg, MainAddressRegQ,nodeAlpha,counterOut, MainAddressRegD,windowCounterSignal);
counterLabel: CounterGrid PORT MAP (clk,rst,enableMainAddressReg,filterRead,windowRead,start,  filterSize , saveResult,enableMainAddressReg,filterAck , dataAck,nodeGamma,counterOut);
border: BorderCircuit GENERIC MAP (n=>18) PORT MAP (stepSize,filterSize,windowCounterSignal,outBorderCircuit); 
Adder25: my_nadder GENERIC MAP (n=>18) PORT MAP (MainAddressRegQ,std_logic_vector(to_unsigned(25,18)),'0',outAdderValue,adderCout); 

WriteAddressAdder: my_nadder GENERIC MAP (n=>18) PORT MAP (nodeAlpha,std_logic_vector(to_unsigned(65560,18)),'0',outWriteAddressAdder,adderCout);

FilterOrWindow: mux2 GENERIC MAP (n=>18) PORT MAP (windowReadAndFalling,MainAddressRegQ,outAdderValue,outReadAddress);

ReadORWrite : mux2 GENERIC MAP (n=>18) PORT MAP (saveResult,outReadAddress,outWriteAddressAdder,outAddressRamSignal);
enableMainAddressReg <=(filterRead OR windowRead);
    
doneCircuitComponent: DoneCircuit PORT MAP (resultAck, filterSize , stepSize,windowCounterSignal,signalDone); 

--save address register
writeLocationRegister:DFlipFlop GENERIC MAP (n=>18)PORT MAP (clk,start,saveResult,writeLocationRegisterD,nodeAlpha);  
Adder1: my_nadder GENERIC MAP (n=>18) PORT MAP (nodeAlpha,std_logic_vector(to_unsigned(1,18)),'0',writeLocationRegisterD,adderCout);
 
windowReadAndFalling<=windowRead;

-- ram 

writeSignalRam <= not (enableMainAddressReg) AND saveResult; 
--myRam: Ram PORT MAP (clk,outAddressRamSignal, dataINRam ,dataOUTRamSig ,writeSignalRam);

counter<=counterOut;
nodeGamma3<=nodeGamma;

 
END DMA;
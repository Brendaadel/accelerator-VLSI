LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.numeric_std.all;
--if indicator=0 ->get 16
--if indicator =1 ->get 32
ENTITY Ram IS
	PORT(
		clk: in std_logic;
		address : IN  std_logic_vector(17 DOWNTO 0);
                dataIN : IN std_logic_vector(7 DOWNTO 0);
		dataOUT : OUT std_logic_vector(39 DOWNTO 0);
		Wr:IN std_logic);
END ENTITY Ram;

ARCHITECTURE RamArch OF Ram IS

	TYPE ram_type IS ARRAY(0 TO 131097) OF std_logic_vector(7 DOWNTO 0);
	SIGNAL ram : ram_type ;
        SIGNAL test : std_logic_vector (7 downto 0);
	BEGIN	
	process(clk,address,dataIN) is
	begin
          --rd-wr = i'm reading
--memory reading , bktb f memory
	
	dataOUT<=ram(to_integer(unsigned(address))+4)&ram(to_integer(unsigned(address))+3)&ram(to_integer(unsigned(address))+2)&ram(to_integer(unsigned(address))+1)&ram(to_integer(unsigned(address)));

	if clk='0' then
		if Wr='1' then
		   ram(to_integer(unsigned(address))) <= dataIN;
		end if;
	end if;	
	end process;
test<=ram(65560);
        	
END RamArch;


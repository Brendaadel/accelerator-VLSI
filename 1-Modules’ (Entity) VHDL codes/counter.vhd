library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

ENTITY counter IS
GENERIC ( n : integer := 3);
PORT( clk,rst,enable,size: IN std_logic;
 outPort : INOUT std_logic_vector(n-1 DOWNTO 0)
);
END counter;


ARCHITECTURE counter_arch OF counter IS
BEGIN

PROCESS (clk,rst,enable)
BEGIN
IF rst = '1' THEN
   outPort <= (OTHERS=>'0');
ELSIF ((clk = '1') AND outPort="100" AND enable='1' AND size='1') THEN
		outPort <= (OTHERS=>'0');
ELSIF ((clk = '1') AND outPort="010" AND enable='1' AND size='0') THEN
		outPort <= (OTHERS=>'0');
ELSIF ((clk = '1') AND enable = '1') THEN 
		outPort <= outPort + 1;
ELSE outPort<=outPort;

END IF;	
END PROCESS;

END counter_arch;




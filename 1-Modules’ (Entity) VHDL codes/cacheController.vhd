LIBRARY IEEE;
USE IEEE.std_logic_1164.all;

ENTITY cacheController IS  
PORT (size , readFilter , readWindow , aluDone:  IN std_logic;
       counter:IN std_logic_vector (2 downto 0);
       regEnables: OUT  std_logic_vector (10 downto 0)
);   
END ENTITY cacheController;


ARCHITECTURE cacheController OF cacheController IS
Signal  counterValue : std_logic_vector (4 downto 0);
BEGIN
     
  regEnables <= (aluDone &"0000000000") WHEN size='0' AND readWindow='0' AND readFilter ='0' ELSE
	   (aluDone &"0000000"&counterValue(2 downto 0)) WHEN size='0' AND readWindow='0' AND readFilter ='1' ELSE
           (aluDone & "00" &counterValue(2 downto 0) &"00000")WHEN size='0' AND readWindow='1' AND readFilter ='0' ELSE
           (aluDone & "0000000000" ) WHEN size='1' AND readWindow='0' AND readFilter ='0' ELSE
           (aluDone & "00000"&counterValue) WHEN size='1' AND readWindow='0' AND readFilter ='1' ELSE 
            (aluDone & counterValue & "00000")WHEN size='1' AND readWindow='1' AND readFilter ='0' ELSE 
            (aluDone &"0000000000");

counterValue <= "00001" when counter="000" ELSE 
                 "00010" WHEN  counter="001" ELSE 
                "00100" WHEN  counter="010" ELSE
                "01000" WHEN counter="011" ELSE 
                "10000" WHEN counter="100" ELSE 
                 "00000"; -- won't happen 

END cacheController;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

ENTITY BoothCounter IS
GENERIC ( n : integer := 3);
PORT( clk,rst: IN std_logic;
 outPort : INOUT std_logic_vector(n-1 DOWNTO 0)
);
END BoothCounter;


ARCHITECTURE BoothCounter_arch OF BoothCounter IS


Signal  x: integer;
BEGIN
PROCESS (clk,rst)
BEGIN
IF rst = '1' THEN
   outPort <= (OTHERS=>'0');
   x<=0;
ELSIF (rising_edge(clk) AND outPort = std_logic_vector(to_unsigned(7,n))) THEN
		outPort <= (OTHERS=>'0');
ELSIF (rising_edge(clk)) THEN 
    x<= x+1;
		outPort <= std_logic_vector(to_unsigned(x,n));
ELSE outPort<=outPort;

END IF;	
END PROCESS;

END BoothCounter_arch;





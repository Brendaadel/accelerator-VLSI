load_library tsmc035_typ
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/DFlipFlop.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/mux2.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/mux4.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/SRLatch.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/SRFlipFlop.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/bit_adder.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/nBitAdder.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/nBitAdderWithOverflow.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/BoothCounter.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/ALU.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/Summator.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/ALuWithSummator.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/handShakes.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/counter.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/CounterGrid.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/DoneCircuit.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/BorderCircuit.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/FSM.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/DMA.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/cacheController.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/cache.vhd }
read -technology "tsmc035_typ"  { /home/ayman/Desktop/vlsi_project/accelerator-VLSI/Accelerator.vhd }
elaborate Accelerator -architecture Accelerator 
pre_optimize .work.Accelerator.Accelerator -common_logic -unused_logic -boundary -xor_comparator_optimize 
pre_optimize .work.Accelerator.Accelerator -extract 
set register2register 100.000000
set input2register 100.000000
set register2output 100.000000
optimize .work.Accelerator.Accelerator -target tsmc035_typ -macro -auto -effort standard -hierarchy preserve  
optimize_timing .work.Accelerator.Accelerator 
set novendor_constraint_file FALSE
auto_write -format Verilog Accelerator.v
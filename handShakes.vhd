LIBRARY IEEE;
USE IEEE.std_logic_1164.all;

ENTITY handShakes IS  
PORT (start,filterAck , dataAck ,done,algo,nodeB,clk ,reset:  IN std_logic;
      readFilter , readWindow ,resultAck , saveResult,aluDn,enableAlu2:  OUT std_logic
       
);   
END ENTITY handShakes;


ARCHITECTURE handShakes OF handShakes IS

COMPONENT SRFlipFlop IS  
PORT (set , reset,clk:IN std_logic;
       OUT1: OUT std_logic
);   

END COMPONENT ;
COMPONENT SRLatch IS  
PORT (set , reset,clk:IN std_logic;
       OUT1: OUT std_logic
);   
END COMPONENT;

signal  clkChoosen,notClk,aluDone ,enableAlu ,resultAckSig , saveResultSig , readWindowSig,filterOrResultAck 
, doneOrDataAck ,dataAckAndNotDone , enableAluAndNodeBOrNotAlgo,filterAckOrReset,doneOrDataAckOrReset,
saveResultOrReset,resultAckSigOrReset,readWindowSigOrReset: std_logic;


BEGIN
notClk<=not(clk); 
filterAckOrReset<=filterAck OR reset;
saveResultOrReset<=saveResultSig or reset;
resultAckSigOrReset<=resultAckSig or reset;


readFilterReg : SRLatch PORT MAP (start,filterAckOrReset,clk,readFilter);
filterOrResultAck<=(filterAck or resultAckSig);

readWindowReg : SRLatch PORT MAP (filterOrResultAck,doneOrDataAckOrReset,clk,readWindowSig);
doneOrDataAckOrReset<=(done or dataAck or reset);
readWindow<= readWindowSig;

enableAluReg : SRFlipFlop PORT MAP (dataAckAndNotDone,saveResultOrReset,clk,enableAlu);
dataAckAndNotDone<=(dataAck AND not (done));


--aluDoneReg : SRLatch PORT MAP (enableAluAndNodeBOrNotAlgo,resultAckSigOrReset,clkChoosen,aluDone);
 --resultAckSig <= saveResultSig AND (clk);
resultAck<=resultAckSig;

---saveResultReg : SRFlipFlop PORT MAP (aluDone,resultAckSigOrReset,clk,saveResultSig);

---------
enableAluAndNodeBOrNotAlgo<= (enableAlu AND (nodeB OR (not(algo) AND notClk))); 
saveResultSig<=enableAluAndNodeBOrNotAlgo AND not(resultAckSigOrReset)  ;
resultAckReg : SRFlipFlop PORT MAP (saveResultSig,readWindowSig,clk,resultAckSig);
readWindowSigOrReset<=  readWindowSig OR reset;
aluDn<=aluDone;


enableAlu2<=enableAlu;
saveResult<=saveResultSig;
aluDone<=saveResultSig;
----


--clkChoosen <= notClk when algo='1'
--else clk; 

END handShakes;

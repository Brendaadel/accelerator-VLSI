library ieee;
use ieee.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
ENTITY CounterGrid IS
PORT( clk,rst,enb,filterRead,windowRead,start,size,saveResult,enableMainAddressReg: IN std_logic;
filterAck , dataAck ,nodeGamma2 : OUT std_logic;
counterOut: OUT std_logic_vector (2 downto 0)
);
END CounterGrid;


ARCHITECTURE CounterGrid OF CounterGrid IS

COMPONENT counter IS
GENERIC ( n : integer := 3);
PORT( clk,rst,enable,size: IN std_logic;
 outPort : INOUT std_logic_vector(n-1 DOWNTO 0)
);
END COMPONENT;

COMPONENT DFlipFlop IS
GENERIC ( n : integer := 32);
PORT( Clk,Rst,EnableDFF  : IN std_logic;
d : IN std_logic_vector(n-1 DOWNTO 0);
 q : OUT std_logic_vector(n-1 DOWNTO 0)
);
END COMPONENT ;

SIGNAL rstSignalCounter, nodeGamma,resetReg1,resetReg2, clkInverted: std_logic;
SIGNAL outSignal : std_logic_vector (2 downto 0);
SIGNAL startVector, qReg1,qReg2 ,nodeGammaVector,saveResultVector : std_logic_vector (0 downto 0);

BEGIN

clkInverted <= not(clk);
counter1: counter GENERIC MAP (n=>3) PORT MAP (clk,rstSignalCounter,enableMainAddressReg,size,outSignal);
counterOut<=outSignal;
nodeGamma <= '1' WHEN (outSignal(0)='0' AND  outSignal(1)='0' AND  outSignal(2)='1' AND size='1') OR  (outSignal(0)='0' AND  outSignal(1)='1' AND  outSignal(2)='0' AND size='0') ELSE '0';
filterAck<= nodeGamma AND clkInverted AND filterRead;
dataAck<= nodeGamma AND clkInverted AND windowRead;

startVector(0)<=start;


reg2:DFlipFlop GENERIC MAP (n=>1) PORT MAP (clk,resetReg2,'1',nodeGammaVector,qReg2); --should work on falling
resetReg2<= '1' when clk='0' AND (outSignal(2)='0' AND outSignal(1)='0' AND outSignal(0)='0')
                 else '0';

nodeGammaVector(0)<=nodeGamma;
rstSignalCounter<= (start OR qReg2(0)or qReg1(0));
reg1: DFlipFlop GENERIC MAP (n=>1)PORT MAP (clk,rst ,'1',saveResultVector,qReg1);
saveResultVector(0)<=saveResult;
nodeGamma2<=nodeGamma;

END CounterGrid;